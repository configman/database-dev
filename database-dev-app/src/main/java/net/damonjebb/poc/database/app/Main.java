package net.damonjebb.poc.database.app;

import net.damonjebb.poc.database.flyway.FlywayDatabaseManager;
import net.damonjebb.poc.database.h2.H2Db;

import org.flywaydb.core.Flyway;

import java.util.HashMap;
import java.util.Map;

public class Main {

  private H2Db h2Db;
  private FlywayDatabaseManager flywayDatabaseManager;
  private Map<String, String> parameters = new HashMap<>();
  private static final Flyway flywayService = new Flyway();

  public static void main(String[] args) {

    H2Db db = new H2Db();
    FlywayDatabaseManager flyway = new FlywayDatabaseManager(flywayService);
    Main runner = new Main();
    runner.run(args, db, flyway);

  }

  public void run(String[] args, H2Db h2Db, FlywayDatabaseManager flywayDatabaseManager) {
    parseParameters(args);

    this.h2Db = h2Db;
    this.flywayDatabaseManager = flywayDatabaseManager;

    h2Db.startDb("9002", true);
    flywayDatabaseManager.setFlywayDatasource(parameters.get("dbUrl"), "appUser", "password");
    flywayDatabaseManager.setFlywayLocations("migrations/h2/test");
    Boolean schemaUpToDate = flywayDatabaseManager.isSchemaUpToDate();
    if (!schemaUpToDate) {
      if (!parameters.containsKey("updateDb")) {
        System.out.println("Error target database needs to be updated before app can run");
      } else {
        flywayDatabaseManager.migrateDatabase();
      }
    }
  }


  private void parseParameters(String[] args) {
    if (args != null && args.length > 0) {
      for (String parameter : args) {
        if (parameter.startsWith("db=")) {
          parameters.put("dbUrl", parameter.substring(3));
        } else if ("--updateDb".equals(parameter)) {
          parameters.put("updateDb", "");
        }
      }
    }
  }

  public H2Db getH2Db() {
    return h2Db;
  }
}
