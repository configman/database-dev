package net.damonjebb.poc.database.flyway;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;

public class FlywayDatabaseManager {

  private Flyway flywayService;

  public FlywayDatabaseManager(Flyway flyway) {
    this.flywayService = flyway;
  }

  public void setFlywayDatasource(String jdbcUrl, String user, String password) {
    flywayService.setDataSource(jdbcUrl, user, password);
    System.out.print("After setDatasource call\n");
  }

  public Boolean isSchemaUpToDate() {
    MigrationInfoService migrationInfoService = flywayService.info();
    MigrationInfo[] migrationsPending = migrationInfoService.pending();
    int numberOfPendingMigrations = migrationsPending.length;

    return (numberOfPendingMigrations == 0);
  }

  public void setFlywayLocations(String locations) {
    flywayService.setLocations(locations);
  }

  public void migrateDatabase() {
    flywayService.migrate();
  }

  public void cleanDatabase() {
    flywayService.clean();
  }

  public void validateDatabase() {
    flywayService.validate();
  }

  public Flyway getFlywayService() {
    return flywayService;
  }
}
