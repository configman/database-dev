package net.damonjebb.poc.database.h2;

import org.h2.tools.Server;
import java.sql.SQLException;
import java.util.ArrayList;

public class H2Db {

  private Server h2Server;

  public void startDb(String port, Boolean allowOthers) {
    try {
      String[] parameters = buildParameterArray(port, allowOthers);

      h2Server = Server.createWebServer(parameters).start();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void stopDb() {
    h2Server.stop();
  }

  private String[] buildParameterArray(String port, Boolean tcpAllowOthers)  {
    ArrayList<String> parameters = new ArrayList<>();
    if (port != null) {
      parameters.add("-webPort");
      parameters.add(port);
    }
    if (tcpAllowOthers) {
      parameters.add("-webAllowOthers");
    }
    return parameters.toArray(new String[0]);
  }

  public Server getH2Server() {
    return h2Server;
  }


  
}
