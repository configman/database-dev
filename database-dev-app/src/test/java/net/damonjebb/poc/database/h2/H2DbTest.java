package net.damonjebb.poc.database.h2;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import net.damonjebb.poc.database.h2.H2Db;


public class H2DbTest {

  @Test
  public void createH2Db() {
    H2Db h2Db = new H2Db();
    h2Db.startDb("9092", true);
    String h2Status = h2Db.getH2Server().getStatus();
    assertTrue("H2 database not created", h2Status.startsWith("Web Console server running at"));
    assertTrue("allow connections not set", h2Status.endsWith("(others can connect)"));
    h2Db.stopDb();
    assertFalse("H2 db not shutdown", h2Db.getH2Server().isRunning(true));
    
  }

  
}
