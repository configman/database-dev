package net.damonjebb.poc.database.flyway;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class FlywayDatabaseManagerTest {

  private final String jdbcUrl = "jdbc:h2:./target/test";
  private final String databaseUser = "test";
  private final Flyway mockFlyway = mock(Flyway.class);
  private final MigrationInfoService mockInfoSvc = mock(MigrationInfoService.class);
  private final MigrationInfo mockInfo = mock(MigrationInfo.class);
  private MigrationInfo[] mockInfoArray = new MigrationInfo[0];

  private final FlywayDatabaseManager flywayDatabaseManager = new FlywayDatabaseManager(mockFlyway);

  @Before
  public void cleanStart() {
    flywayDatabaseManager.setFlywayDatasource(jdbcUrl, databaseUser, databaseUser);
    flywayDatabaseManager.setFlywayLocations("migrations/h2/test");
    flywayDatabaseManager.cleanDatabase();
    when(mockFlyway.info()).thenReturn(mockInfoSvc);

  }


  @Test
  public void flywayStatusNotUpToDate() {

    flywayDatabaseManager.setFlywayDatasource(jdbcUrl, databaseUser, databaseUser);
    when(mockInfoSvc.pending()).thenReturn(new MigrationInfo[1]);
    Boolean schemaStatus = flywayDatabaseManager.isSchemaUpToDate();

    assertFalse("There should be pending migrations", schemaStatus);

  }

  @Test
  public void flywayMigrate() {
    flywayDatabaseManager.migrateDatabase();
    when(mockInfoSvc.pending()).thenReturn(new MigrationInfo[0]);
    Boolean schemaStatus = flywayDatabaseManager.isSchemaUpToDate();
    assertTrue("There should be no pending migrations", schemaStatus);
  }

  @Test
  public void testValidateAfterMigrate() {
    flywayDatabaseManager.migrateDatabase();
    flywayDatabaseManager.validateDatabase();
  }

  @Test(expected = FlywayException.class)
  public void testValidationFailure() {

    doThrow(FlywayException.class).when(mockFlyway).validate();
    flywayDatabaseManager.cleanDatabase();
    flywayDatabaseManager.validateDatabase();
  }
}
