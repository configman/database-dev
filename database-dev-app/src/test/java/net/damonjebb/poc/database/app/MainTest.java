package net.damonjebb.poc.database.app;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import net.damonjebb.poc.database.app.Main;
import net.damonjebb.poc.database.flyway.FlywayDatabaseManager;
import net.damonjebb.poc.database.h2.H2Db;

public class MainTest {

  @Test
  public void runMain() {
    Main main = new Main();

    H2Db mockH2Db = mock(H2Db.class);
    FlywayDatabaseManager mockDbMgr = mock(FlywayDatabaseManager.class);
    when(mockDbMgr.isSchemaUpToDate()).thenReturn(true);
    String[] args = {"db=jdbc:h2:./target/mpg-test"};

    main.run(args, mockH2Db, mockDbMgr);

    verify(mockDbMgr).setFlywayDatasource(args[0].substring(3), "appUser", "password");
    verify(mockDbMgr).setFlywayLocations(anyString());
    verify(mockH2Db).startDb("9002", true);
    verify(mockDbMgr, never()).migrateDatabase();

//    String serverStatus = Main.getH2Db().getH2Server().getStatus();
//    assertTrue("cannot connect to DB", serverStatus.startsWith("Web Console server running at"));
  }

  @Test
  public void testRun__ShouldNotCallMigrate() {
    FlywayDatabaseManager mockDbMgr = mock(FlywayDatabaseManager.class);
    H2Db mockDb = mock(H2Db.class);

    String[] args = {"db=jdbc:h2:./target/mpg-test"};

    Main main = new Main();

    main.run(args, mockDb, mockDbMgr);
    verify(mockDbMgr, never()).migrateDatabase();
  }
  
}
